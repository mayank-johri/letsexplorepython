
## Introducing Some Functional Features

Below are the list of features present in python and which we are going to cover in `functional programming` module.

- Functions as first-class
    - Pure function
    - Higher order functions
        - max() & min()
        - lambda
        - map()
        - filter() 
            - to pass or reject data
            -  to identify outliers
        - iter()
        - sorted()
- Immutable data 
- Evaluation
    - Strict 
    - non-strict
- Recursion & Reductions
    - Simple recursion
        - tail-call optimization
        - tail-call optimization for collections
        - many to one
    - groupby() recutions - many to few
        - mapping by Counter
        - mapping by sorting
        - grouping by key
        - writing file parsers
            - csv file
- Functional type system
- Functions, Iterators & Generators
- Collections  & Functional Programming
    - iterables
    - zip & flatten sequences
    - reversed()
    - enumerate()

> <center>**NOTE**</center>
> 
> If I missed any topic in the below sections, Please let me know at mayankjohri@gmail.com
